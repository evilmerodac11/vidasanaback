const express = require('express');
const app = express();


// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(express.json());

// Routes
app.use(require('./routes/user'));
app.use(require('./routes/products'));
app.use(require('./routes/alimentacion'));
// Starting the server
const server = app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});
module.exports = server;