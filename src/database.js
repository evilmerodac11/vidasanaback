const mysql = require('mysql');
const fs = require('fs');
require('dotenv').config();

class MySqlConnection {
    constructor() {
        // Step 3. Create a connection to the TiDB cluster.
        const options = {
            host: process.env.TIDB_HOST || '127.0.0.1',
            port: process.env.TIDB_PORT || 4000,
            user: process.env.TIDB_USER || 'root',
            password: process.env.TIDB_PASSWORD || '',
            database: process.env.TIDB_DATABASE || 'test',
            ssl: process.env.TIDB_ENABLE_SSL === 'true' ? {
                minVersion: 'TLSv1.2',
                ca: process.env.TIDB_CA_PATH ? fs.readFileSync(process.env.TIDB_CA_PATH) : undefined
            } : null,
        }
        this.connection = mysql.createConnection(options);

    }

    connect() {
        this.connection.connect((error) => {
            if (error) {
                console.log(error);
                return;
            } else {
                console.log('La base se conectó');
            }
        });
    }

    disconnect() {
        this.connection.end((error) => {
            if (error) {
                console.log(error);
                return;
            } else {
                console.log('La conexión se cerró correctamente.');
            }
        });
    }
}

module.exports = MySqlConnection;
