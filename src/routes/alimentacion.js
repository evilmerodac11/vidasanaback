const { Router } = require('express');
const router = Router();

const MySqlConnection = require('../database');



///////////
///de la parte de la tabla producto
///////////

router.get('/api/food/ali/all', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    console.log("get all")
    mysqlConnection.connection.query('select * from tbl_alimentacion', (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.get('/api/food/ali/all/:alimentacion_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    console.log("get by id")

    const { alimentacion_id } = req.params;
    mysqlConnection.connection.query('select * from tbl_alimentacion where alimentacion_id = ?', [alimentacion_id], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    })
});

router.post('/food/ali', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    console.log("post ")

    const { alimentacion_fecha, producto_id, alimentacion_cantidad, usuario_id } = req.body;
    console.log(req.body);
    mysqlConnection.connection.query('insert into tbl_alimentacion(alimentacion_fecha, producto_id, alimentacion_cantidad, usuario_id ) values (?, ?, ?, ?)', [alimentacion_fecha, producto_id, alimentacion_cantidad, usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json({ Status: "Alimentacion guardada" })
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.post('/food/dateanduser', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    console.log("date and user ")

    const { fecha, usuario_id } = req.body;
    console.log(req.body);
    mysqlConnection.connection.query('SELECT * FROM tbl_alimentacion WHERE alimentacion_fecha = ? AND usuario_id = ?', [fecha, usuario_id], (error, rows, fields) => {
        if (!error) {
            console.log(rows);
            res.json(rows);
        } else {
            console.log(error);
            res.status(500).json({ error: 'Ocurrió un error al procesar la solicitud.' });
        }
        mysqlConnection.disconnect();
    });
});

router.put('/api/food/ali/:alimentacion_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    console.log("put alimentacion ")
    const { alimentacion_id } = req.params;
    const { alimentacion_fecha, producto_id, alimentacion_cantidad, usuario_id } = req.body;
    console.log(req.body);
    mysqlConnection.connection.query('update tbl_alimentacion set alimentacion_fecha = ?, producto_id = ?, alimentacion_cantidad = ?, usuario_id = ? where alimentacion_id = ?;',
        [alimentacion_fecha, producto_id, alimentacion_cantidad, usuario_id, alimentacion_id], (error, rows, fields) => {
            if (!error) {
                res.json({ Status: 'Alimentacion actualizada' });
            } else {
                console.log(error);
            }
            mysqlConnection.disconnect();
        });
});

router.delete('/food/ali/:alimentacion_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    console.log("alimentacion delete ")
    console.log(req.params);
    const { alimentacion_id } = req.params;
    mysqlConnection.connection.query('delete from tbl_alimentacion where alimentacion_id = ?', [alimentacion_id], (error, rows, fields) => {
        if (!error) {
            res.json({ Status: "Alimentacion eliminada" });
        } else {
            res.json({ Status: error });
        }
        mysqlConnection.disconnect();
    });
});

module.exports = router;
